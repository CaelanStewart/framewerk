<?php
/**
 * @copyright 2014 Framewerk.io
 * @author Caelan Stewart <caelan@framewerk.io>
 * @license http://www.gnu.org/licenses/gpl-3.0.txt GNU GENERAL PUBLIC LICENSE VERSION 3
 */

if(!defined('APP')) die('direct access forbidden');

define('TIME', date('H:i:s'));
define('DATE', date('Y/m/d'));
define('START_TIME', microtime(true));
define('LOG_ERRORS', true);
define('PRINT_ERRORS', true);

require_once('./System/Kernel.php');

Kernel::ResolveDependencies('config', 'database');

$do = filter_input(INPUT_GET, 'do', FILTER_SANITIZE_SPECIAL_CHARS);

if(empty($do)) {
	$do = '/' . DEFAULT_MODEL;
}

$result = Kernel::AutoRunModel($do);

if(!$result) {
	Kernel::AutoRunModel('/errors/404');
}

Kernel::Log('Execution Finished', 'times');