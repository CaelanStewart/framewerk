<?php if(!defined('APP')) die('Access Denied'); ?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<title>Error 404 - Kangaroo Not Found</title>
	</head>
	<body>
		<h1>Error 404 - Kangaroo Not Found</h1>
		<p>Sorry, but the Kangaroo you were looking for could not be found.</p>
		<footer>
			&copy; 2014&mdash;<?php echo date("Y"); ?> Kanglr
		</footer>
	</body>
</html>