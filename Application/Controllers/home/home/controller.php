<?php

/* controller.home.php (c) 2014 Caelan Stewart */

Kernel::ResolveDependencies('lang', 'view', 'path');

class homeController extends ControllerObject {
	public function __construct() {
		$this->config = [
			'enable-caching' => true,
			'cache-lifetime' => (60)
		];
	}
	
	protected function Execute($view = false) {
		if($view !== false) {
			$view = View::Load($view);
			$view->pageTitle = Lang::Get('home/home');
			$view->views = Database::SQL("SELECT * FROM views")->GetArrayAll();
			
			return $view->GetOutput();
		}
	}
};
