<?php

/* model.php (c) 2014 Caelan Stewart */

Kernel::ResolveDependencies('controller');

global $homeModel, $initFunction;

$initFunction = 'homeInit';

function homeInit($doArray) {
	global $homeModel;
	
	return Kernel::ProcessModel($homeModel, $doArray);
};

$homeModel = [
	'?' => function() {
		Database::Insert()
			->Into('views')
			->Values([
				"page"		=> "home",
				"timestamp"	=> time()
			])
			->Save();
			
		$home = Controller::Load('home/home');
		echo $home->GetOutput();
	}
];