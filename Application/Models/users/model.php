<?php

/* model.php (c) 2014 Caelan Stewart */

global $usersModel, $initFunction;

$initFunction = 'usersInit';

function usersInit($doArray) {
	global $usersModel;
	
	return Kernel::ProcessModel($usersModel, $doArray);
};

$usersModel = [
	'?' => function() {
		// default function
	},
	
	'profile' => [
		'*' => function($uid = 0) {
			/**
			 * if url = /users/profile/57 then $uid = 57
			 */
		}
	]
];