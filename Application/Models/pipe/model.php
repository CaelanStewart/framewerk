<?php
/**
 * pipe/model.php This is used for receiving and processing requests for piped HTML segments (or anything, really).
 * @copyright 2014 Framewerk.io
 * @author Caelan Stewart <caelan@framewerk.io>
 * @license http://www.gnu.org/licenses/gpl-3.0.txt GNU GENERAL PUBLIC LICENSE VERSION 3
 */

Kernel::ResolveDependencies('pipe', 'json');

global $pipeModel, $initFunction;

$initFunction = 'pipeInit';

function pipeInit($doArray) {
	global $pipeModel;
	
	return Kernel::ProcessModel($pipeModel, $doArray);
};

$pipeModel = [
	'load' => function() {
		$path = filter_input(INPUT_POST, 'pipePath', FILTER_SANITIZE_SPECIAL_CHARS);
		if(!is_null($path) && $path !== false) {
			$pipe = Pipe::Load($path);
			if($pipe !== false) {
				echo JSON::Response($pipe);
			}
		}
	}
];
