<?php

if(!defined("APP")) die("Access Denied");

Kernel::ResolveDependencies('view');

global $errorsModel, $initFunction;

$initFunction = 'errorsInit';

function errorsInit($doArray) {
	global $errorsModel;
	
	return Kernel::ProcessModel($errorsModel, $doArray);
};

$errorsModel = array(
	'404' => function() {
		Database::Insert()
			->Into('views')
			->Values([
				"page"		=> "errors/404",
				"timestamp"	=> time()
			])
			->Save();
		
		$error = View::Load('errors/404');
		echo $error->GetOutput();
	}
);
