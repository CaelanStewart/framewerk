<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

/**
 * @copyright 2014 Framewerk.io
 * @author Caelan Stewart <caelan@framewerk.io>
 * @license http://www.gnu.org/licenses/gpl-3.0.txt GNU GENERAL PUBLIC LICENSE VERSION 3
 */

define('APP', 1);
define('DEFAULT_MODEL', 'home');

require_once('./Application/Bootloader.php');