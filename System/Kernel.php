<?php
/**
 * @copyright 2014 Framewerk.io
 * @author Caelan Stewart <caelan@framewerk.io>
 * @license http://www.gnu.org/licenses/gpl-3.0.txt GNU GENERAL PUBLIC LICENSE VERSION 3
 */

if(!defined('APP')) die('direct access forbidden');

/**
 * The Kernel class is the core of Framewerk. It handles the
 * loading/running of dependencies and models. It also contains
 * the functions used for logging errors and run times.
 * 
 * @author Caelan Stewart <caelan@framewerk.io>
 */
class Kernel {
	/**
	 * @var array $loadedClasses is used internally to keep track of loaded classes
	 */
	static private $loadedClasses = [ ];
	
	static private $loadedDrivers = [ ];
	
	
	/**
	 * Logs a message to a file.
	 * @param string $comment is the message to append to the log file.
	 * @param string $log is the file to log to. The default file is error.log.
	 * @return boolean Indicates if the message was logged successfully or not.
	 */
	static public function Log($comment, $log = 'error') {
		$time = microtime(true) - START_TIME;
		$uri = str_replace('/Kanglr/', '', htmlspecialchars($_SERVER['REQUEST_URI']));
		$toLog = implode(' ', array(TIME, DATE, $time, $comment, "({$uri})", PHP_EOL));
		
		if($log === 'error' && PRINT_ERRORS) {
			echo $toLog;
		}
		
		if(file_put_contents("./Logs/{$log}.log", $toLog, FILE_APPEND)) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Runs a Model array and gives the model control of the do array. Models should
	 * follow the guidelines at framewerk.io/guides/models
	 * @param string $name The name of the Model to run.
	 * @param array $doArray The array of instructions (usually from URL) to pass to the models init function.
	 * @return boolean Returns true if the model was found and was run. False, otherwise.
	 */
	static public function RunModel($name, $doArray) {
		$directory = "./Application/Models/{$name}";
		if(is_dir($directory)) {
			$init = "{$directory}/model.php";
			if(is_file($init)) {
				include_once($init);
				global $initFunction;
				if(function_exists($initFunction)) {
					return call_user_func($initFunction, $doArray);
				}
			}
		}
		
		Self::Log("Model '{$name}' does not exist or is not valid.");
		
		return false;
	}
	
	/**
	 * Automatically processes the $_GET['do'] string and runs the model. Models should follow
	 * the guidelines at framewerk.io/guides/models
	 * @param string $do Should contain the raw $_GET['do'] string.
	 * @return boolean Returns true if the model was run. False, otherwise.
	 */
	static public function AutoRunModel($do) {
		$array = array_values(array_filter(explode('/', $do), function($item) { return !empty($item); }));
		
		if(count($array) > 0) {
			$model = $array[0];
			$array = array_slice($array, 1);
			
			if(Self::RunModel($model, $array)) {
				return true;
			}
			
			return false;
		}
		
		if(Self::RunModel(DEFAULT_MODEL, [ ])) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Processes as model tree with the $_GET['do'] input in an array format.
	 * @param array $model The model tree, should follow guidelines found at framewerk.io/guides/models
	 * @param array $doArray Array format of $_GET['do'] input.
	 * @return mixed Returns the return value of the function that the do array resolved to. Or false if
	 * if the do array resolution failed. It also logs an error if resolution faled.
	 */
	static public function ProcessModel($model, $doArray) {
		// Tries default function if the do array is empty.
		if(isset($model['?']) && count($doArray) == 0) {
			$model['?']();
			
			return true;
		}
		
		$fail = false;
		$args = [ ];
		$current = $model;
		foreach($doArray as $k => $v) {
			if(is_array($current)) {
				if(isset($current[$v])) {
					$current = $current[$v];
					continue;
				}
				
				if(isset($current['*'])) {
					$current = $current['*'];
					/* 
					 * Append argument to stack, as this function
					 * has a variable name.
					 */
					$args[ ] = $v;
					continue;
				}
				
				/* An error occurred, mark as a failure. */
				$fail = true;
				break;
			}
			
			/*
			 * Have we found a callable yet?
			 */
			if(is_callable($current)) {
				$args[ ] = $v;
			}
		}
		
		if(!$fail) {
			/* Is there a default function we can use? */
			if(is_array($current) && isset($current['?'])) {
				$current = $current['?'];
			}
			
			/*
			 * Final check to ensure we have a callable in the
			 * variable $current. If so, execute it.
			 */
			if(is_callable($current)) {
				call_user_func_array($current, $args);
				return true;
			}
		}
		
		/* Errors had occured, log and return. */
		Kernel::Log("Kernel::ProcessModel(): Failed to process model with route '" . implode('/', $doArray) . "'.");
		return false;
	}
	
	/**
	 * Loads system and application specific classes. A class should follow the guidelines
	 * at framewerk.io/guides/classes
	 * @param string $name The name of the class to load.
	 * @return boolean If the class exists and was loaded, returns true. False, otherwise.
	 */
	static public function LoadClass($name) {
		if(!in_array($name, Self::$loadedClasses)) {
			$file_Application = "./Application/Classes/class.{$name}.php";
			$file_System = "./System/Classes/class.{$name}.php";
			$file = $file_System;
			if(!is_file($file_System)) {
				$file = $file_Application;
			}
			
			if(!is_file($file)) {
				Self::Log("Class '{$name}' does not exist.");
				return false;
			}
			
			include_once($file);
			self::$loadedClasses[] = $name;
		}
		
		return true;
	}
	
	/**
	 * Automatically resolves dependencies.
	 * @param string|array ... An array of dependencies, or a variable argument list of dependencies.
	 * @return null Returns nothing. Logs an error if a dependency could not be loaded.
	 */
	static public function ResolveDependencies() {
		$dependencies = func_get_args();
		if(count($dependencies) > 0) {
			if(is_array($dependencies[0])) {
				$dependencies = $dependencies[0];
			}
			
			foreach($dependencies as $dependency) {
				if(!Self::LoadClass($dependency)) {
					Self::Log("Could not resolve dependency '{$dependency}'.");
				}
			}
		}
	}
	
	static public function LoadDriver($driver) {
		if(in_array($driver, Self::$loadedDrivers)) {
			return true;
		}
		
		$driverDirSystem	= "./System/Drivers/driver.{$driver}";
		$driverDirApp		= "./Application/Drivers/driver.{$driver}";
		
		if(is_dir($driverDirSystem)) {
			$driverDir = $driverDirSystem;
		} else if(is_dir($driverDirApp)) {
			$driverDir = $driverDirApp;
		}
		
		if(isset($driverDir)) {
			$driverFile = "{$driverDir}/driver.php";
			$driverConf = "{$driverDir}/config.php";
			
			if(is_file($driverFile) && is_file($driverConf)) {
				$conf = include($driverConf);
				include_once($driverFile);
				
				if(function_exists($conf['init-function'])) {
					$args = [ ];
					if(isset($conf['args'])) {
						$args = $conf['args'];
					}
					
					call_user_func_array($conf['init-function'], $args);
				}
				
				Self::$loadedDrivers[] = $driver;
				
				return $conf;
			}
		}
		
		Kernel::Log("Kernel::LoadDriver(): Driver '{$driver}' does not exist or is not valid.");
		return false;
	}
};
