<?php
/**
 * @copyright 2014 Framewerk.io
 * @author Caelan Stewart <caelan@framewerk.io>
 * @license http://www.gnu.org/licenses/gpl-3.0.txt GNU GENERAL PUBLIC LICENSE VERSION 3
 */

if(!defined('APP')) die('direct access forbidden');

global $database_MySQL_Connection;

class Database_MySQL_Base {
	private $connection;
	protected $sql = '';
	
	public function __construct() {
		global $database_MySQL_Connection;
		$this->connection = $database_MySQL_Connection;
	}
	
	public function Execute($sql) {
		$args = func_get_args();
		for($i = 1; $i < count($args); $i++) {
			$sql = str_replace('$' . $i, $this->Escape($args[$i]), $sql);
		}
		$result = mysqli_query($this->connection, $sql);
		$err = mysqli_error($this->connection);
		if(strlen($err) > 0) {
			Kernel::Log("Database_MySQL_Base::Execute(): MySQL Error: '{$err}'");
		}
		return $result;
	}
	
	public function Escape($str) {
		return mysqli_real_escape_string($this->connection, $str);
	}
	
	public function GetSQL() {
		return $this->sql;
	}
};

class Database_MySQL_Select extends Database_MySQL_Base {
	private $result = null;
	private $where = false;
	private $select = false;
	private $from = false;
	private $errors = false;
	private $args = [ ];
	
	public function Select($default = '*') {
		$this->sql .= 'SELECT';
		$cols = func_get_args();
		$colCount = count($cols);
		$i = 1;
		foreach($cols as $col) {
			$this->sql .= ' ' . $this->Escape($col) . (($i < $colCount) ? ',' : '');
			$i++;
		}
		
		$this->select = true;
		
		return $this;
	}
	
	public function QuickSelect($columns, $table, $condition = null) {
		$this->sql .= "SELECT {$columns} FROM {$table}" . (!is_null($condition) ? " WHERE {$condition} " : ' ');
		
		if(!is_null($condition)) {
			$this->where = true;
		}
		
		$this->select = true;
		
		return $this;
	}
	
	public function From($table) {
		if(!$this->select) {
			Kernel::Log("Database_MySQL_Select::From(): must make a selection before setting 'FROM'.");
			$this->errors = true;
			
			return $this;
		}
		
		if(empty($table)) {
			Kernel::Log("Database_MySQL_Select::From(): table name cannot be empty.");
			
			return $this;
		}
		
		$this->sql .= ' FROM ' . $table;
		
		$this->from = true;
		
		return $this;
	}
	
	public function Where($where, $expectation = null, $operator = '=') {
		if($this->errors) {
			return $this;
		}
		
		if(!$this->from) {
			Kernel::Log("Database_MySQL_Select::Where(): Must set 'FROM' before setting 'WHERE'.");
			$this->errors = true;
			
			return $this;
		}
		
		if(!is_null($expectation)) {
			$this->sql .= implode(' ', [' ', 'WHERE', $where, $operator, "'" . $this->Escape($expectation) . "'"]);
		} else {
			if(!is_array($where)) {
				throw new Exception('Database_MySQL_Query::Where(): First argument should be an array of comparisons if there are several comparisons.');
			}
			$i = 1;
			foreach($where as $column => $value) {
				if($i == 1 && !$this->where) {
					$this->sql .= ' WHERE';
				} else {
					$this->sql .= ' AND';
				}
				
				$this->sql .= implode(' ', [' ', $column, $operator, "'" . $this->Escape($value) . "'"]);
			}
		}
		
		$this->where = true;
		
		return $this;
	}
	
	public function Limit($start, $amount) {
		if($this->errors) {
			return $this;
		}
		
		if(!$this->from) {
			Kernel::Log("Database_MySQL_Select::Limit(): Must set 'FROM' before setting 'LIMIT'.");
			$this->errors = true;
			
			return $this;
		}
		
		$this->sql .= "LIMIT {$this->Escape($start)}, {$this->Escape($amount)}";
		
		return $this;
	}
	
	public function SQL($sql) {
		$args = func_get_args();
		$sql = $args[0];
		$this->args = array_slice($args, 1);
		$this->sql = $sql;
		
		$this->errors = false;
		
		return $this;
	}
	
	public function GetArray($arrayType = MYSQLI_ASSOC) {
		if(is_null($this->result)) {
			$argArray = $this->args;
			array_unshift($argArray, $this->sql);
			$this->result = call_user_func_array([$this, 'Execute'], $argArray);
		}
		
		return mysqli_fetch_array($this->result, $arrayType);
	}
	
	public function GetArrayAll($arrayType = MYSQLI_ASSOC) {
		if(is_null($this->result)) {
			$argArray = $this->args;
			array_unshift($argArray, $this->sql);
			$this->result = call_user_func_array([$this, 'Execute'], $argArray);
		}
		
		$array = [ ];
		while($row = mysqli_fetch_array($this->result, $arrayType)) {
			$array[ ] = $row;
		}
		
		return $array;
	}
	
	public function GetResult() {
		if(is_null($this->result)) {
			$argArray = $this->args;
			array_unshift($argArray, $this->sql);
			$this->result = call_user_func_array([$this, 'Execute'], $argArray);
		}
		
		return $this->result;
	}
};

class Database_MySQL_Delete extends Database_MySQL_Select {
	public function Delete() {
		$this->sql .= 'DELETE';
		
		return $this;
	}
};

class Database_MySQL_Create extends Database_MySQL_Base {
	private $ifNotExists;
	private $name;
	private $engine;
	private $result = null;
	private $columns = [ ];
	private $primaryKey = null;
	
	public function Table($name, $ifNotExists = true, $engine = 'InnoDB') {
		$this->ifNotExists = $ifNotExists;
		$this->name = $name;
		$this->engine = $engine;
		
		return $this;
	}
	
	public function AddColumn($name, $type) {
		$this->columns[$name] = $type;
		
		return $this;
	}
	
	public function SetPrimaryKey($col) {
		$this->primaryKey = $col;
		
		return $this;
	}
	
	public function SetFullText($cols) {
		$this->sql .= 'FULLTEXT(' . $this->Escape(implode(', ', $cols)) . '), ';
		$this->engine = 'MyISAM';
		
		return $this;
	}
	
	public function Save() {
		$columns = '';
		$columnsCount = count($this->columns);
		$i = 0;
		foreach($this->columns as $column => $type) {
			$i++;
			$columns .= "{$this->Escape($column)} {$this->Escape($type)}" . ($i < $columnsCount ? ',' : '');
		}
		if(!is_null($this->primaryKey)) {
			$columns .= ",PRIMARY KEY({$this->Escape($this->primaryKey)})";
		}
		$this->sql = 'CREATE TABLE' . ($this->ifNotExists ? ' IF NOT EXISTS ' : ' ') . $this->Escape($this->name) . ' (' . $columns . ') ENGINE=' . $this->Escape($this->engine);
		$this->result = $this->Execute($this->sql);
		
		return $this;
	}
	
	public function GetResult() {
		if(is_null($this->result)) {
			$this->Save();
		}
		
		return $this->result;
	}
};

class Database_MySQL_Insert extends Database_MySQL_Base {
	private $result = null;
	
	public function Into($table) {
		$this->sql .= "INSERT INTO {$this->Escape($table)} ";
		
		return $this;
	}
	
	public function Values($assocArray) {
		$columns = implode(',', array_keys($assocArray));
		$values = '';
		$valuesCount = count($assocArray);
		$i = 0;
		foreach($assocArray as $value) {
			$i++;
			$values .= "'{$this->Escape($value)}'" . ($i < $valuesCount ? ',' : '');
		}
		
		$this->sql .= "({$columns}) VALUES ({$values})";
		
		return $this;
	}
	
	public function Save() {
		$this->result = $this->Execute($this->sql);
		
		return $this;
	}
	
	public function GetResult() {
		if(is_null($this->result)) {
			$this->Save();
		}
		
		return $this->result;
	}
};

class Database_MySQL_Update extends Database_MySQL_Select {
	private $where = false;
	private $result = null;
	private $errors = false;
	private $table = false;
	private $set = false;
	
	public function Table($name) {
		if(empty($name)) {
			$this->errors = true;
			Kernel::Log("Database_MySQL_Update::Table(): table name cannot be empty.");
			
			return $this;
		}
		
		$this->sql .= "UPDATE {$this->Escape($name)} ";
		
		$this->table = true;
		
		return $this;
	}
	
	public function Set() {
		if($this->errors) {
			return $this;
		}
		
		if(!$this->table) {
			$this->errors = true;
			Kernel::Log("Database_MySQL_Update::Set(): table name must be set before setting 'SET'.");
			
			return $this;
		}
		
		$args = func_get_args();
		if(count($args) == 1 && is_array($args[0])) {
			$this->sql .= 'SET ';
			$i = 0;
			$colCount = count($args[0]);
			foreach($args[0] as $column => $value) {
				$i++;
				$this->sql .= "{$this->Escape($column)} = '{$this->Escape($value)}'" . ($i < $colCount ? ', ' : ' ');
			}
			
			$this->set = true;
		} else if(count($args) == 2) {
			$this->sql .= "SET {$this->Escape($args[0])} = '{$this->Escape($args[1])}'";
			
			$this->set = true;
		} else {
			Kernel::Log("Database_MySQL_Update::Set(): invalid arguments passed.");
			$this->errors = true;
		}
		
		return $this;
	}
	
	public function Where($where, $expectation = null, $operator = '=') {
		if(!is_null($expectation)) {
			$this->sql .= implode(' ', [' ', 'WHERE', $where, $operator, "'" . $this->Escape($expectation) . "'"]);
		} else {
			if(!is_array($where)) {
				throw new Exception('Database_MySQL_Query::Where(): First argument should be an array of comparisons if there are several comparisons.');
			}
			$i = 1;
			foreach($where as $column => $value) {
				if($i == 1 && !$this->where) {
					$this->sql .= ' WHERE';
				} else {
					$this->sql .= ' AND';
				}
				
				$this->sql .= implode(' ', [' ', $column, $operator, "'" . $this->Escape($value) . "'"]);
			}
		}
		
		$this->where = true;
		
		return $this;
	}
	
	public function Save() {
		if($this->errors) {
			return $this;
		}
		
		$this->result = $this->Execute($this->sql);
		
		return $this;
	}
	
	public function GetResult() {
		if(is_null($this->result)) {
			$this->Save();
		}
		
		return $this->result;
	}
};

class Database_MySQL_Drop extends Database_MySQL_Base {
	private $result;
	
	public function Table($name) {
		$this->result = $this->Execute('DROP TABLE ' . $this->Escape($name));
		
		return $this;
	}
	
	public function GetResult() {
		return $this->result;
	}
};

function Database_MySQL_Init($config) {
	global $database_MySQL_Connection;
	$connection = mysqli_connect($config['host'], $config['username'], $config['password']);
	mysqli_select_db($connection, $config['database']);
	$database_MySQL_Connection = $connection;
}

/**
 * Usage:
 * $result = Database::Query()->Select()->From('users')->Where('banned', '1')->Execute();
 * $result = Database::QuickSelect('*', 'users', 'banned = 1');
 *
 * $result = Database::Query()->QuickSelect('*', 'users', 'banned = 1')->GetArrayAll();
 */
