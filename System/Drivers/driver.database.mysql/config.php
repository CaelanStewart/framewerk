<?php
/**
 * @copyright 2014 Framewerk.io
 * @author Caelan Stewart <caelan@framewerk.io>
 * @license http://www.gnu.org/licenses/gpl-3.0.txt GNU GENERAL PUBLIC LICENSE VERSION 3
 */

if(!defined('APP')) die('direct access forbidden');

Kernel::ResolveDependencies('config');

$config = [
	"init-function"		=> "Database_MySQL_Init",
	"args"				=> [Config::GetOrDefault("database", ["host" => "localhost", "username" => "root", "password" => "", "database" => "framewerk"])],
	"map"				=> [
		"Select"			=> "Database_MySQL_Select",
		"QuickSelect"		=> "Database_MySQL_Select",
		"Delete"			=> "Database_MySQl_Delete",
		"CreateTable"		=> "Database_MySQL_Create",
		"DropTable"			=> "Database_MySQL_Drop",
		"Insert"			=> "Database_MySQL_Insert",
		"Update"			=> "Database_MySQL_Update",
		"SQL"				=> "Database_MySQL_Select",
		"Base"				=> "Database_MySQL_Base"
	]
];

return $config;