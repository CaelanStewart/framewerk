<?php
/**
 * @copyright 2014 Framewerk.io
 * @author Caelan Stewart <caelan@framewerk.io>
 * @license http://www.gnu.org/licenses/gpl-3.0.txt GNU GENERAL PUBLIC LICENSE VERSION 3
 */

if(!defined('APP')) die('direct access forbidden');

Kernel::ResolveDependencies('path');

class URL {
	static private $rootURL;
	
	static public function Init() {
		Self::$rootURL = Config::GetOrDefault('server/root-url', htmlspecialchars(strip_tags($_SERVER['SERVER_NAME'])));
	}
	
	static public function Model($path) {
		$pathArray = Path::Decode($path);
		array_unshift($pathArray, '/');
		
		$url = Self::$rootURL . implode('/', $pathArray);
		return $url;
	}
};

URL::Init();