<?php
/**
 * @copyright 2014 Framewerk.io
 * @author Caelan Stewart <caelan@framewerk.io>
 * @license http://www.gnu.org/licenses/gpl-3.0.txt GNU GENERAL PUBLIC LICENSE VERSION 3
 */

if(!defined('APP')) die('direct access forbidden');

Kernel::ResolveDependencies('path', 'config');

class Cache {
	static private $cache;
	static private $directory;
	
	static public function Init() {
		Self::$directory = Config::GetOrDefault('cache/directory', './Application/Cache');
		if(!is_dir(Self::$directory)) {
			Kernel::Log('Cache::Init(): Default cache location \'' . Self::$directory . '\' does not exist. Please specify a directory in the configuration, or ensure the default directory exists.');
			throw new Exception('Cache::Init(): Default cache location \'' . Self::$directory . '\' does not exist. Please specify a directory in the configuration, or ensure the default directory exists.');
		}
	}
	
	/**
	 * Continue docs
	 */
	static public function Save($path, $content, $lifetime = 0) {
		if($lifetime == 0) {
			/* default lifetime */
			$lifetime = (60 * 60 * 24);
		}
		
		$pathArray = Path::Decode($path);
		$directory = array_merge([Self::$directory], $pathArray);
		
		$current = [ ];
		foreach($directory as $dir) {
			$current[ ] = $dir;
			$imploded = implode('/', $current);
			if(!is_dir($imploded)) {
				mkdir($imploded);
			}
		}
		
		$directoryString = implode('/', $directory);

		if(!is_dir($directoryString)) {
			Kernel::Log("Cache::Save(): Invalid path '{$path}'");
			return false;
		}
		
		$configFile = "{$directoryString}/config.php";
		
		$config = [ ];
		$config['expiry'] = time() + $lifetime;
		$config['content'] = serialize($content);
		
		$contents = '<?php if(!defined("APP")) die("direct access forbidden"); ';
		
		$contents .= 'return ' . var_export($config, true) . ';';
		
		if(file_put_contents($configFile, $contents) !== false) {
			return true;
		}
		
		return false;
	}
	
	static public function Load($path) {
		$path = implode('/', Path::Decode($path));
		$directory = Self::$directory;
		$directory = "{$directory}/{$path}";
		$configFile = "{$directory}/config.php";
		if(is_file($configFile)) {
			$config = include($configFile);
			
			if($config['expiry'] > time()) {
				if(isset($config['content'])) {
					return unserialize($config['content']);
				}
			}
		}
		
		Kernel::Log("Cache::Load(): Could not load cache from path '{$path}'");
		return false;
	}
	
	static public function Exists($path) {
		$path = implode('/', Path::Decode($path));
		$directory = Self::$directory;
		$directory = "{$directory}/{$path}";
		$configFile = "{$directory}/config.php";
		
		return is_file($configFile);
	}
};

Cache::Init();
