<?php
/**
 * @copyright 2014 Framewerk.io
 * @author Caelan Stewart <caelan@framewerk.io>
 * @license http://www.gnu.org/licenses/gpl-3.0.txt GNU GENERAL PUBLIC LICENSE VERSION 3
 */

if(!defined('APP')) die('Direct access forbidden');

Kernel::ResolveDependencies('cache', 'path', 'config');

/**
 * Used as a base of a Controller. Controllers should follow the guidelines at framewerk.io/guides/controllers
 * @author Caelan Stewart <caelan@framewerk.io>
 */
abstract class ControllerObject {
	/**
	 * @var string $config This should be set by the controller being executed.
	 * The config should follow the guidlines at framewerk.io/guides/controllers/config
	 */
	protected $config = array();
	
	private $directory;
	private $path;
	
	public function __Init($directory, $path) {
		$this->directory = $directory;
		$this->path = $path;
	}
	
	/**
	 * Used to run the controller. This function handles the caching of controller
	 * output, and should be used instead of calling Execute() directly.
	 * @param boolean $view A view is passed into this function. To learn more about views, please visit framewerk.io/guides/views.
	 */
	public function GetOutput($view = false) {
		if($view === false) {
			$view = $this->path;
		}

		$cachePath = "controller/{$this->path}";
		if(isset($this->config['enable-caching']) && $this->config['enable-caching'] == true) {
			if(Cache::Exists($cachePath)) {
				$cached = Cache::Load($cachePath);
				if($cached !== false) {
					return $cached;
				}
			}
		}
		
		$output = $this->Execute($view);
		
		if(isset($this->config['enable-caching']) && $this->config['enable-caching'] == true) {
			$lifetime = (60 * 60);
			if(isset($this->config['cache-lifetime'])) {
				$lifetime = $this->config['cache-lifetime'];
			}
			
			Cache::Save($cachePath, $output, $lifetime);
		}
		
		return $output;
	}
	
	abstract protected function Execute($view = false);
};


/**
 * Static class containing functions for loading and using controllers.
 * Visit framewerk.io/guides/controllers to learn more about controllers.
 */
class Controller {
	static private $directory;
	
	static public function Init() {
		/* use custom directory, or use default if no custom directory is configured. */
		Controller::$directory = Config::GetOrDefault('controller/directory', './Application/Controllers');
	}
	
	static public function Load($path) {
		$pathArray = Path::Decode($path);
		array_unshift($pathArray, Controller::$directory);
		$name = $pathArray[count($pathArray) - 1];
		$directory = implode('/', $pathArray);
		$file = $directory . '/controller.php';
		if(is_file($file)) {
			include_once($file);
			$className = $name . 'Controller';
			if(class_exists($className)) {
				$instance = new $className();
				$instance->__Init($directory, $path);
				
				return $instance;
			}
		}
		
		Kernel::Log("Controller::Load(): Could not load controller '{$path}'.");
		return false;
	}
	
	static public function Run($path, $view = false) {
		$cInstance = Self::Load($path);
		if($cInstance !== false) {
			if($view === false) {
				$view = $path;
			}
			return $cInstance->GetOutput($view);
		}
		
		Kernel::Log("Controller::Run(): There was an error running controller '{$path}'.");
		return false;
	}
};

Controller::Init();
