<?php
/**
 * @copyright (c) 2014 Framewerk.io
 * @author Caelan Stewart <caelan@framewerk.io>
 * @license http://www.gnu.org/licenses/gpl-3.0.txt GNU GENERAL PUBLIC LICENSE VERSION 3
 */

if(!defined('APP')) die('direct access forbidden');

Kernel::ResolveDependencies('config', 'path', 'cache');

class Pipe {
	static private $pipeDirectory;
	
	static public function Init() {
		Self::$pipeDirectory = Config::GetOrDefault('pipe/pipe-directory', './Application/Pipes');
	}
	
	static public function Load($path) {
		$cache = false;
		$pipePath = str_replace('..', '', Self::$pipeDirectory . "/{$path}");
		if(is_dir($pipePath)) {
			$configFile = "{$pipePath}/config.php";
			$pipeFile = "{$pipePath}/pipe.php";
			if(is_file($pipeFile)) {
				if(is_file($configFile)) {
					$config = include($configFile);
					if($config['enable-caching'] === true) {
						$cache = true;
						$cached = Cache::Get("pipe/{$path}");
						if($cached !== false) {
							return $cached;
						}
					}
				}
				
				include($pipeFile);
				if(isset($pipeInitFunction)) {
					$output = $pipeInitFunction();
					if($cache) {
						$lifetime = (60 * 60);
						if(isset($config['cache-lifetime'])) {
							$lifetime = $config['cache-lifetime'];
						}
						Cache::Save("pipe/{$path}", $output, $lifetime);
					}
					
					return $output;
				}
			}
		}
		
		Kernel::Log("Pipe::Load(): The pipe {$path} does not exist.");
		return false;
	}
};

Pipe::Init();