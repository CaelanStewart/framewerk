<?php
/**
 * @copyright 2014 Framewerk.io
 * @author Caelan Stewart <caelan@framewerk.io>
 * @license http://www.gnu.org/licenses/gpl-3.0.txt GNU GENERAL PUBLIC LICENSE VERSION 3
 */

if(!defined('APP')) die('direct access forbidden');

Kernel::ResolveDependencies('path');

/**
 * The Config class is used for loading configuration files and getting specific values.
 * @author Caelan Stewart <caelan@framewerk.io>
 */
class Config {
	/**
	 * @var array $config is used internally to cache loaded config files to save them
	 * from being loaded again on each config request.
	 */
	static private $config;
	
	/**
	 * Used to load an entire config file as an array.
	 * @param string $category The category/name of the config file to load.
	 * @param boolean $logErrors Do you want to log errors? This is handy for a number of reasons.
	 * @return array|boolean Returns the config array or false if it failed to load.
	 */
	static public function Load($category, $logErrors = true) {
		if(isset(Self::$config[$category])) {
			return Self::$config[$category];
		}
		
		$file = "./Application/Config/{$category}.php";
		if(is_file($file)) {
			Self::$config[$category] = include_once($file);
			return Self::$config[$category];
		}
		
		if($logErrors) {
			Kernel::Log("Config::Load(): invalid/non-existent config category '{$category}'");
		}
		
		return false;
	}
	
	/**
	 * Used to get a specific configuration entry.
	 * @param string $path This should be formatted like so: category/option.
	 * @param array $defaults These are the default configuration options. So if an option does not exist in a config
	 * file, but it does in the defaults, the default option will be returned.
	 * @return returns the option, or false on failure. It also also logs an error on failure if $logErrors === true.
	 */
	static public function Get($path, $defaults = [ ], $showErrors = true) {
		$pathArray = Path::Decode($path);
		list($category, $name) = array_pad($pathArray, 2, false);
		if(($category !== false && $name !== false) || !empty($defaults)) {
			$config = Self::Load($category, empty($defaults));
			$config = array_merge($defaults, ($config === false) ? [ ] : $config);
			if(isset($config[$name])) return $config[$name];
		} else if(count($pathArray) == 1) {
			$config = Self::Load($category, empty($defaults));
			$config = array_merge($defaults, ($config === false) ? [ ] : $config);
			return $config;
		}
		
		Kernel::Log("Config::Get(): invalid/non-existent path: '{$path}'");
		
		return false;
	}
	
	static public function GetOrDefault($path, $default) {
		$pathArray = Path::Decode($path);
		list($category, $name) = array_pad($pathArray, 2, false);
		if($category !== false && $name !== false) {
			$config = Self::Load($category, false);
			if(isset($config[$name])) {
				return $config[$name];
			}
		} else if(count($pathArray) == 1) {
			$config = Self::Load($category, false);
			if($config !== false) {
				return $config;
			}
		}
		
		return $default;
	}
};