<?php
/**
 * @copyright 2014 Framewerk.io
 * @author Caelan Stewart <caelan@framewerk.io>
 * @license http://www.gnu.org/licenses/gpl-3.0.txt GNU GENERAL PUBLIC LICENSE VERSION 3
 */

if(!defined('APP')) die('direct access forbidden');

class JSON {
	static public function Response($data = null, $errors = false, $message = 'OK') {
		$array = array(
			"data"		=> $data,
			"errors"	=> $errors,
			"message"	=> $message
		);
		
		return json_encode($array);
	}
	
	static public function Error($message) {
		return JSON::Response(null, true, $message);
	}
};