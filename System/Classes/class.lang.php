<?php
/**
 * @copyright 2014 Framewerk.io
 * @author Caelan Stewart <caelan@framewerk.io>
 * @license http://www.gnu.org/licenses/gpl-3.0.txt GNU GENERAL PUBLIC LICENSE VERSION 3
 */

if(!defined('APP')) die('direct access forbidden');

Kernel::ResolveDependencies('path', 'config');

/**
 * The Lang class is used for loading language files for use in multi-lingual applications.
 * @author Caelan Stewart <caelan@framewerk.io>
 */
class Lang {
	/**
	 * @var array $lang is used internally to cache loaded lang files to save them
	 * from being loaded again on each lang request.
	 */
	static private $lang;
	
	static private $defaultLanguage;
	
	static public function Init() {
		Self::$defaultLanguage = Config::GetOrDefault('lang/default-language', 'en_GB');
	}
	
	/**
	 * Used to load an entire lang file as an array.
	 * @param string $category The category/name of the lang file to load.
	 * @return array|boolean Returns the lang array or false if it failed to load.
	 */
	static public function Load($language, $category) {
		if(isset(Self::$lang[$language]) && isset(Self::$lang[$language][$category])) {
			return Self::$lang[$language][$category];
		}
		
		$file = "./Application/Lang/{$language}/lang.{$category}.php";
		
		if(is_file($file)) {
			Self::$lang[$language][$category] = include_once($file);
			return Self::$lang[$language][$category];
		}
		
		Kernel::Log("Lang::Load(): invalid/non-existent lang category '{$category}'");
		
		return false;
	}
	
	/**
	 * Used to get a specific language entry.
	 * @param string $path This should be formatted like so: category/option, or language/category/option.
	 * @return bool|string Returns the option, or false on failure. It also also logs an error on failure if $logErrors === true.
	 */
	static public function Get($path) {
		$decoded = Path::Decode($path);
		if(count($decoded) == 2) {
			array_unshift($decoded, Self::$defaultLanguage);
		}
		
		if(count($decoded) == 3) {
			list($language, $category, $name) = $decoded;
			if(($category !== false && $name !== false) || !empty($defaults)) {
				$lang = Self::Load($language, $category);
				if(isset($lang[$name])) return $lang[$name];
			}
		}
		
		Kernel::Log("Lang::Get(): invalid/non-existent path: '{$path}'");
		
		return false;
	}
};

Lang::Init();
