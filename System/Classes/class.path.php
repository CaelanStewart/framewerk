<?php
/**
 * @copyright 2014 Framewerk.io
 * @author Caelan Stewart <caelan@framewerk.io>
 * @license http://www.gnu.org/licenses/gpl-3.0.txt GNU GENERAL PUBLIC LICENSE VERSION 3
 */

if(!defined('APP')) die('direct access forbidden');

class Path {
	static private $separator;
	
	static public function Init() {
		$separator = '/';
		if(defined("DEFAULT_PATH_SEPARATOR")) {
			$separator = DEFAULT_PATH_SEPARATOR;
		}
		
		Self::$separator = $separator;
	}
	
	static public function Decode($path) {
		$array = explode(Self::$separator, $path);
		return $array;
	}
	
	static public function Encode() {
		$array = func_get_args();
		if(count($array) > 0) {
			if(is_array($array[0])) {
				$array = $array[0];
			}
			
			return implode(Self::$separator, $array);
		}
	}
};

Path::Init();