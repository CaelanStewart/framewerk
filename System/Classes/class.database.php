<?php
/**
 * @copyright 2014 Framewerk.io
 * @author Caelan Stewart <caelan@framewerk.io>
 * @license http://www.gnu.org/licenses/gpl-3.0.txt GNU GENERAL PUBLIC LICENSE VERSION 3
 */

if(!defined('APP')) die('direct access forbidden');

class Database {
	static private $init = false;
	static public $driverConfig;
	static public $type;
	static public $base;
	
	static public function Init() {
		if(Self::$init) return;
		$type = Config::GetOrDefault("database/type", "mysql");
		Self::$driverConfig = Kernel::LoadDriver("database.{$type}");
		Self::$type = $type;
		$base = new Self::$driverConfig['map']['Base']();
		Self::$init = true;
	}
	
	static public function Select($default = '*') {
		$args = func_get_args();
		$obj = new Self::$driverConfig['map']['Select']();
		return call_user_func_array($obj->Select, $args);
	}
	
	static public function QuickSelect($columns, $table, $condition = null) {
		$obj = new Self::$driverConfig['map']['QuickSelect']();
		return $obj->QuickSelect($columns, $table, $condition);
	}
	
	static public function Delete() {
		$obj = new Self::$driverConfig['map']['Delete']();
		return $obj->Delete();
	}
	
	static public function Create() {
		return new Self::$driverConfig['map']['CreateTable']();
	}
	
	static public function Drop() {
		return new Self::$driverConfig['map']['DropTable']();
	}
	
	static public function Insert() {
		return new Self::$driverConfig['map']['Insert']();
	}
	
	static public function Update() {
		return new Self::$driverConfig['map']['Update']();
	}
	
	static public function SQL($sql) {
		$obj = new Self::$driverConfig['map']['SQL']();
		
		if(is_array($sql)) {
			$type = Self::$type;
			if(!isset($sql[$type])) {
				Kernel::Log("Database::Execute(): No SQL provided for type '{$type}'.");
				return false;
			}
			
			return call_user_func_array([$obj, 'SQL'], $sql[$type]);
		}
		$args = func_get_args();
		return call_user_func_array([$obj, 'SQL'], $args);
	}
};

Database::Init();