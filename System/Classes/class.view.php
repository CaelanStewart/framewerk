<?php
/**
 * @copyright 2014 Framewerk.io
 * @author Caelan Stewart <caelan@framewerk.io>
 * @license http://www.gnu.org/licenses/gpl-3.0.txt GNU GENERAL PUBLIC LICENSE VERSION 3
 */

if(!defined('APP')) die('Direct access forbidden');

Kernel::ResolveDependencies('cache', 'path', 'file', 'config');

class View_Object {
	private $view;
	private $path;
	private $config;
	private $vars = [ ];
	
	public function __construct($view, $path, $config) {
		$this->view = $view;
		$this->path = $path;
		$this->config = $config;
	}
	
	public function __set($name, $value) {
		$this->vars[$name] = $value;
	}
	
	public function __get($name) {
		if(isset($this->vars[$name])) {
			return $this->vars[$name];
		}
		
		Kernel::Log("View_Object Instance: property {$name} does not exist.");
		throw new Exception("View_Object Instance: property {$name} does not exist.");
	}
	
	public function __isset($name) {
		return isset($this->vars[$name]);
	}
	
	public function __unset($name) {
		if(isset($this->vars[$name])) {
			unset($this->vars[$name]);
		}
		
		Kernel::Log("View_Object Instance: property {$name} does not exist.");
		throw new Exception("View_Object Instance: property {$name} does not exist.");
	}
	
	public function GetOutput($allowPHP = null) {
		$output = null;
		
		if($this->config['enable-caching']) {
			$path = implode('/', $this->path);
			$cachePath = "view/{$path}";
			if(Cache::Exists($cachePath)) {
				$cached = Cache::Get($cachePath);
				if($cached !== false) {
					return $cached;
				}
			}
		}
		
		if(isset($this->config['allow-php']) && is_null($allowPHP)) {
			$allowPHP = $this->config['allow-php'];
		}
		
		if(is_null($allowPHP)) {
			$allowPHP = true;
		}
		
		if($allowPHP) {
			$output = File::GetPHP($this->view, $this->vars);
		} else {
			$output = File::GetText($this->view);
		}
		
		if($this->config['enable-caching']) {
			$lifetime = (60 * 60);
			if(isset($this->config['cache-lifetime'])) {
				$lifetime = $this->config['cache-lifetime'];
			}
			
			Cache::Save($cachePath, $output, $lifetime);
		}
		
		return $output;
	}
};

class View {
	static private $directory;
	
	static public function Init() {
		$directory = Config::Get('view/directory', ['directory' => './Application/Views']);
		Self::$directory = $directory;
		if(!is_dir(Self::$directory)) {
			Kernel::Log("View::Init(): Default view directory '{$directory}' does not exist. Please specify a folder for the location of view files.");
			throw new Exception("View::Init(): Default view directory '{$directory}' does not exist. Please specify a folder for the location of view files.");
		}
	}
	
	static public function Load($path) {
		$path = Path::Decode($path);
		
		array_unshift($path, Self::$directory);
		$directory = implode('/', $path);
		$view = $directory . '/view.php';
		$configFile = $directory = '/config.php';
		
		$config = [
			"enable-caching" => false
		];
		
		if(is_file($configFile)) {
			$config = include($configFile);
		}
		
		if(is_file($view)) {
			return new View_Object($view, $path, $config);
		}
		
		Kernel::Log("View::Load(): View file '{$view}' could not be loaded, no such file or directory.");
		return null;
	}
	
	static public function Get($path, $vars = [ ]) {
		$view = Self::Load($path);
		
		if(!is_null($view)) {
			foreach($vars as $key => $value) {
				$view->__set($key, $value);
			}

			return $view->GetOutput();
		}
		
		Kernel::Log("View::Get(): Failed to load view '{$path}'.");
		return null;
	}
};

View::Init();
