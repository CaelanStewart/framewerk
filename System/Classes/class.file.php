<?php
/**
 * @copyright 2014 Framewerk.io
 * @author Caelan Stewart <caelan@framewerk.io>
 * @license http://www.gnu.org/licenses/gpl-3.0.txt GNU GENERAL PUBLIC LICENSE VERSION 3
 */

if(!defined('APP')) die('Direct access forbidden');

Kernel::ResolveDependencies('config', 'path');

class File {
	static private $rootURL;
	
	static public function Init() {
		Self::$rootURL = Config::GetOrDefault('server/root-url', htmlspecialchars(strip_tags($_SERVER['SERVER_NAME'])));
	}
	
	static public function GetJSON($file, $array = true) {
		if(is_file($file)) {
			$file = file_get_contents($file);
			if($file !== false) {
				return json_decode($file, $array);
			}
		}
		
		Kernel::Log("File::GetJSON(): Could not load JSON file '{$file}'.");
		return false;
	}
	
	static public function GetPHP($file, $var = [ ]) {
		if(is_file($file)) {
			ob_start();
			
			include($file);
			
			$output = ob_get_contents();
			ob_end_clean();
			
			return $output;
		}
		
		Kernel::Log("File::GetPHP(): Could not load PHP file '{$file}'.");
		return false;
	}
	
	
	static public function GetText($file) {
		if(is_file($file)) {
			$output = file_get_contents($file);
			
			return $output;
		}
		
		Kernel::Log("File::GetText(): Could not load text file '{$file}'.");
		return false;
	}
	
	static public function GetURL($file) {
		$path = explode('/', $file);
		if($path[0] == '.') {
			$path = array_slice($path, 1);
		}
		
		array_unshift($path, Self::$rootURL);
		
		$path = implode('/', $path);
		
		return $path;
	}
};
