<?php

/**
 * @copyright 2014 Framewerk.io
 * @author Caelan Stewart <caelan@framewerk.io>
 * @license http://www.gnu.org/licenses/gpl-3.0.txt GNU GENERAL PUBLIC LICENSE VERSION 3
 */

error_reporting(E_ALL);
ini_set('display_errors', 1);

define('APP', 1);
define('DEFAULT_MODEL', 'home');
define('TIME', date('H:i:s'));
define('DATE', date('Y/m/d'));
define('START_TIME', microtime(true));
define('LOG_ERRORS', true);
define('PRINT_ERRORS', true);

require_once('./System/Kernel.php');

Kernel::ResolveDependencies('config', 'database');

Database::Create()
	->Table('views')
	->AddColumn('id', 'INT NOT NULL AUTO_INCREMENT')
	->AddColumn('page', 'VARCHAR(256)')
	->AddColumn('timestamp', 'INT')
	->SetPrimaryKey('id')
	->Save();

Kernel::Log('Installer Execution Finished', 'times');
